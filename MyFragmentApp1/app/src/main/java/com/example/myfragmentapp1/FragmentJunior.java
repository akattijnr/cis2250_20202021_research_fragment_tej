package com.example.myfragmentapp1;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentJunior#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentJunior extends Fragment {


    //Defining global variables
    private ToggleButton toggleButton;
    private TextView textView;
    private SeekBar seekBar;
    private ImageView imageView;
    private Button button;
    private EditText editText;
    private Toast toast;


    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public FragmentJunior() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentJunior.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentJunior newInstance(String param1, String param2) {
        FragmentJunior fragment = new FragmentJunior();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public static FragmentJunior newInstance() {
        FragmentJunior fragment = new FragmentJunior();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_junior, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //Using findViewById to get the seek_bar_scroll
        textView = view.findViewById(R.id.seek_bar_scroll);

        //Setting toggle textView to invisible at the start
        textView.setVisibility(View.INVISIBLE);

        imageView = view.findViewById(R.id.imageView);
        imageView.setVisibility(view.INVISIBLE);

        //Using findViewById to get the toggleButton
        toggleButton = view.findViewById(R.id.toggleButton);

        // using setOnCheckedChangeListener() method
        toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Log.d("Eli clicked", "ON is clicked");

                    //Setting a text if toggle button is pressed
                    textView.setText("Toggle On");

                    //Setting the color of the text
                    textView.setTextColor(0xFF00FF00);

                    //Making text visible
                    textView.setVisibility(View.VISIBLE);

                    //Setting image visible
                    imageView.setVisibility(view.VISIBLE);

                } else {
                    Log.d("Eli clicked", "OFF is clicked");
                    textView.setText("Toggle Off");
                    textView.setVisibility(View.VISIBLE);

                    //Setting image invisible
                    imageView.setVisibility(view.INVISIBLE);

                }
            }

        });

        textView = view.findViewById(R.id.seek_bar_message);
        seekBar = view.findViewById(R.id.seekBar);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                textView.setTextSize(progress + 1);

                //Setting transparency for imageView in the seekBar progress bar change event.
                //https://programmer.help/blogs/using-seekbar-drag-bar-to-change-image-transparency-in-android.html
                imageView.setImageAlpha(progress);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        textView = view.findViewById(R.id.valid_email_textView);
        editText = view.findViewById(R.id.editText);
        button = view.findViewById(R.id.button);

        //https://www.c-sharpcorner.com/UploadFile/1e5156/how-to-validate-email/
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String getText = editText.getText().toString();
                String Expn =
                        "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

                //Showing appropriate message whether the email is valid or not.
                if (getText.matches(Expn) && getText.length() > 0) {
                    textView.setText("valid email");
                    toast.makeText(getContext(), "Valid Email", Toast.LENGTH_LONG).show();

                } else {
                    textView.setVisibility(View.INVISIBLE);
                    toast.makeText(getContext(), "Invalid Email", Toast.LENGTH_LONG).show();

                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

        });

    }

}


