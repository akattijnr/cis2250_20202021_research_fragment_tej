package com.example.research_tej;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button buttonRed;
    private Button buttonBlue;
    private Button buttonTariq;
    private EditText password;
    private Button btnSubmit;
    private final FragmentManager fragmentManager = getSupportFragmentManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addListenerOnButton();
    }
        public void addListenerOnButton() {

            password = (EditText) findViewById(R.id.txtPassword);
            btnSubmit = (Button) findViewById(R.id.btnSubmit);

            btnSubmit.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    Toast.makeText(MainActivity.this, password.getText(),
                            Toast.LENGTH_SHORT).show();

                }

            });

        Button getRating = findViewById(R.id.getRating);
        final RatingBar ratingBar = findViewById(R.id.rating);
        getRating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String rating = "Rating is :" + ratingBar.getRating();
                Toast.makeText(MainActivity.this, rating, Toast.LENGTH_LONG).show();
            }
        });

        //
        CamperViewModel model = new ViewModelProvider(this).get(CamperViewModel.class);
        model.getSelectedCamper().observe(this, camper -> {
            // update UI
            Log.d("BJM activity observed","camper="+camper.toString());
            if(camper.getLastName().contains("4")){
                Log.d("bjm","clicked four times...changing fragment to green");
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frameLayoutFragment, FragmentTariq.newInstance(), null);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

            }



        });



        buttonRed = findViewById(R.id.buttonFragmentRed);
        buttonBlue = findViewById(R.id.buttonFragmentBlue);
        buttonTariq = findViewById(R.id.buttonFragmentTariq);

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameLayoutFragment, FragmentRed.newInstance());
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

        //Add listeners for buttons
        buttonRed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction .setCustomAnimations(
                        R.anim.slide_in,  // enter
                        R.anim.fade_out,  // exit
                        R.anim.fade_in,   // popEnter
                        R.anim.slide_out  // popExit
                );
                fragmentTransaction.replace(R.id.frameLayoutFragment, FragmentRed.newInstance(), null);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        buttonBlue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frameLayoutFragment, FragmentBlue.newInstance());
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
        buttonTariq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frameLayoutFragment, FragmentTariq.newInstance());
                //fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }

        });

    }

}